# Create a table that contains 4 columns: zip code, full name, phone number, and email.
# Populate the table with 10000 rows.
# Use pandas and Python.

# Import pandas
import pandas as pd
import random

# Create a table with 4 columns
table1 = pd.DataFrame(columns=['Zip', 'Full Name', 'Phone Number', 'Email'])

# Populate the table with 10000 rows
for i in range(10000):
    table1.loc[i] = [random.randint(60000, 64999), 'Name' + str(i), '913-123-45' + str(i), 'name' + str(i) + '@gmail.com']

# Save table1 as an csv file
table1.to_csv('table1.csv', index=False)
