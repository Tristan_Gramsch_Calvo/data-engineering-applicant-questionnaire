# Compare python-docx-template with python-docx
# Read a csv file called "table1.csv"
import csv
import docx
import docxtpl

with open('table1.csv', 'r') as csvfile:
    reader = csv.reader(csvfile)
    table1 = list(reader)

# Produce word documents for docx libraries.
docx_document = docx.Document()
# Transform table 1 to a string
table1_string = str(table1)
# Add table1 as a string to both template
docx_document.add_paragraph(table1_string)
# .docx documents must exist beforehand to use package docxtpl.
docxtpl_document = docxtpl.DocxTemplate('docxtpl_document.docx')
# Render table1_string to docxtpl_document
context = { 'table1' : table1_string }
docxtpl_document.render(context)
docxtpl_document.save("generated_doc.docx")
# Test performace of both libraries
import timeit
print("docx: ", timeit.timeit("docx_document.save('docx_document.docx')", setup="from __main__ import docx_document", number=1))
print("docxtpl: ", timeit.timeit("docxtpl_document.save('docxtpl_document.docx')", setup="from __main__ import docxtpl_document", number=1))

# Library python-docx has more complete documentation and more repository activity. It is more mature.
# Library python-docx-template fills already produced .docx documents that must contain tags in the form of "{{ variable }}". python-docx-template is not intuitive and hard to scale.
# I produced a data set containing variables 'Zip', 'Full Name', 'Phone Number', 'Email'. The data set contains 10000 rows.
# I produced word documents using libraries python-docx and python-docx-template. The word documents contain a the data table as a literal string.
# Speed test results:
# python-docx:  0.018860526999560534 seconds.
# python-docx-template:  0.010868077999475645 seconds.
# python-docx-template is faster and produces a smaller .docx file.
# Boston has arround 300.000 housing units. Even if we produce a .docx file for each housing unit, performance and memory should not be an issue.
# I recommend using python-docx. It is easier to use and has a more complete API.