# Create a word file from a google sheets document.
# Use library python-docx
# Use Python 3.8
import docx
import pandas as pd

# Create a table that contains 4 columns: zip code, full name, phone number, and email.
# Use pandas.
table1 = pd.DataFrame(columns=['Zip', 'Full Name', 'Phone Number', 'Email'])

# Populate the table with 10 rows
# There should be no duplicate values
table1.loc[1] = ['64106', 'Molly Smith', '913-123-4567', 'msmith@gmail.com']
table1.loc[2] = ['64107', 'James Smith', '913-123-4568', 'jsmith@gmail.com']
table1.loc[3] = ['64108', 'Jane Doe', '913-123-4569', 'jdoe@gmail.com']
table1.loc[4] = ['64109', 'Mike Johnson', '913-123-4570', 'mjohnson@gmail.com']
table1.loc[5] = ['64110', 'Mary Johnson', '913-123-4571', 'mj@gmail.com']
table1.loc[6] = ['64115', 'Molly Johnson', '913-123-4572', 'mollyj@gmail.com']
table1.loc[7] = ['64125', 'Molly Johnson The Second', '913-123-4573', 'molly2@gmail.com']
table1.loc[8] = ['64135', 'Molly Johnson The Third', '913-123-4574', 'molly3smith@gmail.com']
table1.loc[9] = ['64145', 'Molly Johnson The Fourth', '913-123-4575', 'm4smith@gmail.com']
table1.loc[10] = ['64155', 'Molly Johnson The Fifth', '913-123-4576', 'ms5mith@gmail.com']

# table1 has an error. The column 'Zip' should have a leading zero.
# Produce a definition that adds a leading zero to the zip code.
# Use pandas.
table1['Zip'] = table1['Zip'].apply(lambda x: '0' + str(x))
# Print the table
print(table1)

# Create a table that contains adress data that can be joined with table1 using the zip code.
# Use pandas.
table2 = pd.DataFrame(columns=['Street', 'City', 'State', 'Zip'])
# Select desired variables from the signup table left joined with address data using Zip.
# Produce summary statistics by neighborhood.
# Produce a public facing Python API to navigate the data and embedd the API in an application. Use Flask or FastApi to build the API. Use Dash to build the application.